# Templated Page List

Contributors: Colby Sollars, Milo Jennings
Tags: 
Requires at least: 3.0
Tested up to: 3.2.1
Version: 0.9.3
Stable tag: 0.9.3

Highly flexible and customizable method of displaying pages and subpages

## Todo

- Use generic method of acquiring custom image sizes, rather than plugin specific method currently used
- Fix issue that occurs when listing pages that include a page that contains the short code that initiates the function

### Usage

#### Quicktag

`[templated_page_list template="template_name" parent_id="id_number"]`

For use anywhere quick tags are accepted. Will display the output of the template using either the current page's child pages or the `parent_id`specified.

#### Templates

The plugin searches for templates in the subfolder named `page-list-templates` in the active theme's folder. Just place a php file there with your custom template and call it out using the quick tag omitting the .php extension

#### Available Template Tags

 - `{title}`
 - `{excerpt}`
 - `{content}`
 - `{page_id}`
 - `{permalink}`
 - `{image_title}`
 - `{image_caption}`
 - `{image_alt} `
 - `{image_description}`
 - `{image_title}`
 - `{image_url}`
 - `{image_url_image_size}`
 - `{image_url_##x##}`

#### Structural Template Tags

 - `{container_start}`
 - `{container_end}`
 - `{page}`
  - option: repeat  `{page repeat="2"}`

#### Template Structure

```php
{container_start}<ul>{/container_start}

    {page}<li class="page_{page_id}">{title}</li>{/page}
    
{container_end}</ul>{/container_end}
```

## Changelog

### Version 0.9.3 [10/01/2011]

 - added simple-list and thumb-list templates

### Version 0.9.2 [05/19/2011]

 - added {script} and {style} tags for adding more awesomeness to your template
 - added support for full images size template tag

### Version 0.9.1 [03/2011]

 - Initial commit