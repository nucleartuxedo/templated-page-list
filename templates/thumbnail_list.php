{container_start}
<ul class="thumbnail-page-list">{/container_start}
    
        {page}
        <li class="child-page-{page_id}">
            <a href="{permalink}"><img src="{image_url_thumbnail}" alt="{title}" title="{title}"/></a>
        </li>
        {/page}
        
{container_end}
</ul>{/container_end}
