{container_start}<div id="jgal_container">{/container_start}
{page repeat="2" id="test"}
<div id="subpage_{page_id}" style="border:1px solid red">
	<h1 class="the_title">{title}</h1>
	<i class="the_excerpt">{excerpt}</i>
	<img src="{image_url_thumbnail}" /><br />
	<img src="{image_url_medium}" /><br />
	<img src="{image_url_large}" /><br />
	<p class="subpage_content">{content}</p>
	{page_id}
	<p><a href="{permalink}">Permalink</a></p>
	{image_url_10x10}
	{image_url_20x40}
	{image_url_1000x1000}
	{image_url}
	{image_url_full}
	{image_title}
	{image_caption}
	{image_alt}
	{image_description}
</div>
{/page}
{page}
<div style="border:1px solid blue" id="subpage_{page_id}">
	<h1 class="the_title">{title}</h1>
	<i class="the_excerpt">{excerpt}</i>
	<img src="{image_url_thumbnail}" /><br />
	<img src="{image_url_medium}" /><br />
	<img src="{image_url_large}" /><br />
	<p class="subpage_content">{content}</p>
	{page_id}
	<p><a href="{permalink}">Permalink</a></p>
	{image_url_10x10}
	{image_url_20x40}
	{image_url_1000x1000}
	{image_url}
	{image_title}
	{image_caption}
	{image_alt}
	{image_description}
</div>
{/page}
{container_end}</div>{/container_end}
