{container_start}
<ul class="simple-page-list">{/container_start}
	
	{page}
	<li class="child-page-{page_id}">
		<a href="{permalink}">{title}</a>
	</li>
	{/page}
	
{container_end}
</ul>{/container_end}
