<?php

/*
Plugin Name: Templated Page List
Description: Highly flexible and customizable method of displaying pages and subpages
Version: 0.9.3
Author: Colby Sollars
Contributor: Milo Jennings
Author URI: http://www.nucleartuxedo.com
License: GPL2
*/

class subpage_template
{
	var $repeat = 1;
	var $id = "";
	var $template_content = "";
	function subpage_template($content, $attrs=Null)
	{
		$this->template_content = $content;
		if($attrs)
		{
			if(array_key_exists("id", $attrs))
				$this->id = $attrs["id"];
			if(array_key_exists("repeat", $attrs))
				$this->repeat = intval($attrs["repeat"]);
		}
	}
	
	function apply_to_post($post)
	{
		setup_postdata($post);
		$subpage_content = do_shortcode(get_the_content());
		$subpage_content = apply_filters("the_content", $subpage_content);
		$new_post_content = str_replace("{title}", get_the_title(), $this->template_content);
		$new_post_content = str_replace("{excerpt}", get_the_excerpt(), $new_post_content);
		$new_post_content = str_replace("{content}", $subpage_content, $new_post_content);
		$new_post_content = str_replace("{page_id}", $post->ID, $new_post_content);
		$new_post_content = str_replace("{permalink}", get_permalink($post->ID), $new_post_content);
		$new_post_content = str_replace("{subpage_obj}", print_r($post, True), $new_post_content);
		
		$thumb_id = get_post_thumbnail_id($post->ID);
		if(!$thumb_id)
		{
			$attachments = get_posts(array(
				"post_type"=>"attachment",
				"post_parent"=>$post->ID
			));
			
			$thumb_id = count($attachments) ? $attachments[0]->ID : Null;
		}
			
		//find custom image sizes
		preg_match_all('/\{image_url_([0-9]+x[0-9]+)\}/', $new_post_content, $matches);
		$image_urls_custom = $matches[1];
		
		if($thumb_id)
		{
			$image_url = wp_get_attachment_image_src($thumb_id);
			$image_url = $image_url[0];
			$image_thumbnail = wp_get_attachment_image_src($thumb_id, 'thumbnail');
			$image_thumbnail = $image_thumbnail[0];
			$image_medium = wp_get_attachment_image_src($thumb_id, 'medium');
			$image_medium = $image_medium[0];
			$image_large = wp_get_attachment_image_src($thumb_id, 'large');
			$image_large = $image_large[0];
			$image_full = wp_get_attachment_image_src($thumb_id, 'full');
			$image_full = $image_full[0];
			foreach($image_urls_custom as $size)
			{
				$img_info = wp_get_attachment_image_src($thumb_id, explode("x", $size));
				$new_post_content = str_replace("{image_url_$size}", $img_info[0], $new_post_content);
			}
			
			$ais_sizes = get_option("ais_sizes", array());
			$ais_size_names = array_keys($ais_sizes);
			foreach($ais_size_names as $size)
			{
				$img_url = wp_get_attachment_image_src($thumb_id, $size);
				$img_url = $img_url[0];
				$new_post_content = str_replace("{image_url_$size}", $img_url, $new_post_content);
			}
			
			$new_post_content = str_replace("{image_url}", $image_url, $new_post_content);
			$new_post_content = str_replace("{image_url_thumbnail}", $image_thumbnail, $new_post_content);
			$new_post_content = str_replace("{image_url_medium}", $image_medium, $new_post_content);
			$new_post_content = str_replace("{image_url_large}", $image_large, $new_post_content);
			$new_post_content = str_replace("{image_url_full}", $image_full, $new_post_content);
			$new_post_content = str_replace("{image_title}", $thumb_post->post_title, $new_post_content);
			$new_post_content = str_replace("{image_caption}", $thumb_post->post_excerpt, $new_post_content);
			$new_post_content = str_replace("{image_description}", $thumb_post->post_description, $new_post_content);
			$new_post_content = str_replace("{image_alt}", get_post_meta($thumb_id, "_wp_attachment_image_alt", True), $new_post_content);
		} else {
			foreach($image_urls_custom as $size)
			{
				$new_post_content = str_replace("{image_url_$size}", "", $new_post_content);
			}
			$new_post_content = str_replace("{image_url}", "", $new_post_content);
			$new_post_content = str_replace("{image_url_thumbnail}", "", $new_post_content);
			$new_post_content = str_replace("{image_url_medium}", "", $new_post_content);
			$new_post_content = str_replace("{image_url_large}", "", $new_post_content);
			$new_post_content = str_replace("{image_title}", "", $new_post_content);
			$new_post_content = str_replace("{image_caption}", "", $new_post_content);
			$new_post_content = str_replace("{image_description}", "", $new_post_content);
			$new_post_content = str_replace("{image_alt}", "", $new_post_content);
		}
		
		return $new_post_content;
	}
};

class template_iterator
{
	var $templates = array();
	var $current_template = 0;
	var $current_template_reps = 0;
	function template_iterator()
	{
		// constructor
	}
	
	function add_template($template)
	{
		$this->templates[] = $template;
	}
	
	function get_next_template()
	{
		if($this->current_template_reps < $this->templates[$this->current_template]->repeat)
		{
			$this->current_template_reps++;
		}
		else
		{
			$this->current_template++;
			$this->current_template_reps = 1;
		}
		if($this->current_template > count($this->templates)-1)
			$this->current_template = 0;
		return $this->templates[$this->current_template];
	}
};

function parse_tpl_attrs($string)
{
	$parts = explode(" ", $string);
	$attrs = array();
	foreach($parts as $part)
	{
		if(!$part) continue;
		$pair = explode("=", $part);
		$key = $pair[0];
		$value = str_replace("'", "", $pair[1]);
		$value = str_replace('"', "", $value);
		$attrs[$key] = $value;
	}
	return $attrs;
}

function templated_page_list($attrs)
{
	//pauses "Exclude Pages" so excluded pages are included in listing
	if( function_exists("pause_exclude_pages") ) pause_exclude_pages();
	global $post;
	$parent_post = &$post;
	extract(shortcode_atts(array(
		"parent_id"	=> $post->ID,
		"template"	=> "default"
		),
		$attrs
	));
	$template = str_replace("..", "", $template);
	$template = str_replace("/", "", $template);
	$temp_path = STYLESHEETPATH . "/page-list-templates/" . "$template.php";
	if(!is_file($temp_path))
		$temp_path = WP_PLUGIN_DIR . "/templated-page-list/templates/" . $template . ".php";
	ob_start();
	require($temp_path);
	$tpl_content = ob_get_clean();
	$tpl_content = str_replace("\n", "", $tpl_content);
	
	$content = "";
	
	$tpl_vars = array();
	$tpl_patterns = array(
		"tpl_gal_start" => "container_start",
		"tpl_gal_end" => "container_end"
	);
	foreach($tpl_patterns as $key => $tag)
	{
		$tpl_vars[$key] = preg_match("/\{".$tag."\}(.*?)\{\/".$tag."\}/", $tpl_content, $matches) ? $matches[1] : "";
	}
	extract($tpl_vars);
	
	preg_match_all("/\{page( .*?=[\"'].*?[\"'])?\}(.*?)\{\/page\}/", $tpl_content, $tpl_matches);
	
	$tpl_iter = new template_iterator();
	for($i=0; $i<count($tpl_matches[2]); $i++)
	{
		$tpl_content = $tpl_matches[2][$i];
		$tpl_attrs = parse_tpl_attrs($tpl_matches[1][$i]);
		$tpl_iter->add_template(new subpage_template($tpl_content, $tpl_attrs));
	}
	$subpage_templates = $new_matches[2];
	
	$content.=$tpl_gal_start;
	$tmp_post = $post;
	foreach(get_posts(array(
		"post_parent"=>$parent_id,
		"post_type"=>"page",
		"numberposts"=>-1,
		"orderby"=>"menu_order",
		"order"=>"ASC")) as $post)
	{
		$template = $tpl_iter->get_next_template();
		$content.= $template->apply_to_post($post);
	}
	$content.= $tpl_gal_end;
	
	$post = $tmp_post;
	return $content;
	//resumes "Exclude Pages" for normal operation
	if( function_exists("resume_exclude_pages") ) resume_exclude_pages();
}

function get_templates_from_content($content)
{
	$templates = array("default");
	preg_match_all('/\[templated_page_list(.*?)\]/', $content, $shortcode_matches);
	if(count($shortcode_matches) > 2) return $templates;
	
	$templates = array();
	foreach($shortcode_matches[1] as $sc_params)
	{
		preg_match_all('/((.*?)=[\'\"](.*?)[\'\"])/', $sc_params, $param_matches);
		if(count($param_matches) > 4) continue;
		
		foreach($param_matches[2] as &$key) $key = str_replace(" ", "", $key);
		$keys = array_flip($param_matches[2]);
		if(!array_key_exists("template", $keys)) continue;
		if(!in_array($param_matches[3][$keys["template"]], $templates)) $templates[] = $param_matches[3][$keys["template"]];
	}
	
	if(!count($templates)) return array("default");
	return $templates;
}

function get_template_tag_content($template, $tag)
{
	$template = str_replace("..", "", $template);
	$template = str_replace("/", "", $template);
	$temp_path = STYLESHEETPATH . "/page-list-templates/" . "$template.php";
	if(!is_file($temp_path))
		$temp_path = WP_PLUGIN_DIR . "/templated-page-list/templates/" . $template . ".php";
	ob_start();
	require($temp_path);
	$tpl_content = ob_get_clean();
	$tpl_content = str_replace("\n", "", $tpl_content);
	preg_match("/\{".$tag."\}(.*?)\{\/".$tag."\}/", $tpl_content, $tpl_matches);
	if(count($tpl_matches) > 1)
		return $tpl_matches[1];
}

add_shortcode('templated_page_list', 'templated_page_list');

add_action('wp_print_styles', 'templated_page_list_styles');
add_action('wp_print_scripts', 'templated_page_list_scripts');

function templated_page_list_styles() {
	global $post;
	$content = $post->post_content;
	$templates = get_templates_from_content($content);
	foreach($templates as $template)
		echo get_template_tag_content($template, "style");
}

function templated_page_list_scripts() {
	global $post;
	$content = $post->post_content;
	$templates = get_templates_from_content($content);
	foreach($templates as $template)
		echo get_template_tag_content($template, "script");
}